---------------------------------------------------------------------------------
--
--  Entity:       fetch
--  Filename:     fetch.vhd
--  Description:  the Instruction Fetch (IF) unit for
--                the TUD MB-Lite implementation
--
--  Author:       Huib Lincklaen Arriens
--                Delft University of Technology
--                Faculty EEMCS, Department ME&CE, Circuits and Systems
--  Date:         September, 2010
--  Modified:
--  Remarks:
--
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE WORK.mbl_pkg.all;

--------------------------------------------------------------------------------
ENTITY fetch IS
--------------------------------------------------------------------------------
	PORT
	(
		prog_cntr_i :  IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		inc_pc_i    :  IN STD_LOGIC;
		EX2IF_i     :  IN EX2IF_Type;
		IF2ID_o     : OUT IF2ID_Type
	);
END ENTITY fetch;

--------------------------------------------------------------------------------
ARCHITECTURE rtl OF fetch IS
--------------------------------------------------------------------------------

BEGIN

p_fetch:
	PROCESS ( prog_cntr_i, inc_pc_i, EX2IF_i )
		VARIABLE next_pc_v : STD_LOGIC_VECTOR (31 DOWNTO 0);
		VARIABLE incVal_v  : STD_LOGIC_VECTOR (31 DOWNTO 0);
	BEGIN
		incVal_v := X"0000000" & '0' & inc_pc_i & "00";
		ep_add32nc ( prog_cntr_i, incVal_v, '0', next_pc_v );
		IF (EX2IF_i.take_branch = '0') THEN
			IF2ID_o.program_counter <= next_pc_v;
		ELSE
			IF2ID_o.program_counter <= EX2IF_i.branch_target;
		END IF;
	END PROCESS;

END ARCHITECTURE rtl;
