/* Converts bin file to mem file */

#include <stdio.h>
#include <string.h>

void print_help(char * name)
{
	fprintf(stderr, "%s converts a binary file into a mem-file\n", name);
	fprintf(stderr, "Usage: %s INFILE OUTFILE\n", name);
}

int main(int argc, char *argv[])
{
	FILE *infile, *outfile;
	int whi, wlo, bhi, blo;
	unsigned int addr;

	if (argc != 3)
	{
		print_help(argv[0]);
		return(1);
	}

	infile = fopen(argv[1], "rb");
	if (!infile)
	{
		printf("Cannot open file %s\n", argv[1]);
		return(1);
	}

	outfile = fopen(argv[2], "w");
	if (!outfile)
	{
		printf("Cannot open file %s\n", argv[2]);
		return(1);
	}

	fprintf(outfile,"// memory data file (do not edit the following line - required for mem load use)\n"
	                "// format=hex addressradix=h dataradix=h version=1.0 wordsperline=1\n");

	addr = 0;
	while ((whi = fgetc(infile)) != EOF)
	{
		fprintf(outfile,"@%x ", addr);

		if ((wlo = fgetc(infile)) == EOF)
			break;
		if ((bhi = fgetc(infile)) == EOF)
			break;
		if ((blo = fgetc(infile)) == EOF)
			break;

		fprintf(outfile, "%.2x%.2x%.2x%.2x\n", whi,wlo,bhi,blo);
		addr++;
	}

	fclose(infile);
	fclose(outfile);

	return 0;
}
